"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

from gnuradio import gr 
import pmt
import numpy as np

RPRT_0 = [0x52,0x50,0x52,0x54,0x20,0x30,0x0A] #No error, operation completed sucessfully
RPRT_1 = [0x52,0x50,0x52,0x54,0x20,0x31,0x0A] #Invalid parameter
RPRT_6 = [0x52,0x50,0x52,0x54,0x20,0x36,0x0A] #IO error
RPRT_16 = [0x52,0x50,0x52,0x54,0x20,0x31,0x36,0x0A] #Invalid VFO

#pmt.to_pmt({'antenna': ant_msg, 'gain': gain_msg, 'chan': 0, 'freq': freq_msg}) 
def process_cmd(cmd):
    cmd_list = cmd.split(" ")
    if cmd_list[0] == "f":
        print(msg_block.rx_freq)

class msg_block(gr.sync_block):
     def __init__(self, rx_freq=int(436025000), tx_freq=int(145850000)):
         gr.sync_block.__init__(
             self,
             name="Rigctl parser",
             in_sig=None,
             out_sig=None)
 
         self.message_port_register_out(pmt.intern('set_TX'))
         self.message_port_register_out(pmt.intern('set_RX'))
         self.message_port_register_out(pmt.intern('rprt'))
         self.message_port_register_in(pmt.intern('in'))
         self.set_msg_handler(pmt.intern('in'), self.handle_msg)

         self.rx_freq = rx_freq
         self.tx_freq = tx_freq

     def process_cmd(self, cmd):
        if len(cmd) <= 3:
            self.message_port_pub(pmt.intern('rprt'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(RPRT_16), RPRT_16)))
            return
        #print(cmd)
        cmds = cmd.split('\n')

        for i in range(len(cmds)):
           actual_cmd = cmds[i]
           #print(actual_cmd)
           actual_cmd = actual_cmd.strip()
           cmd_list = actual_cmd.split(' ')
           #print(cmd)
           #print(cmd_list)
           if cmd_list[0] == 'f':
               if cmd_list[1].lower() == 'tx':
                   freq_bytes = list(bytes(str(int(self.tx_freq)).encode('ascii')))
                   freq_bytes += (('\n').encode('ascii'))
                   self.message_port_pub(pmt.intern('rprt'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(freq_bytes), freq_bytes)))
                   continue
               elif cmd_list[1].lower() == 'rx':
                   freq_bytes = list(bytes(str(int(self.rx_freq)).encode('ascii')))
                   freq_bytes += (('\n').encode('ascii'))
                   self.message_port_pub(pmt.intern('rprt'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(freq_bytes), freq_bytes)))
                   continue
               else:
                   self.message_port_pub(pmt.intern('rprt'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(RPRT_16), RPRT_16)))
                   continue
           elif cmd_list[0] == 'F':
               if len(cmd_list) < 3:
                   if cmd_list[1].isnumeric():
                       self.message_port_pub(pmt.intern('rprt'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(RPRT_16), RPRT_16)))
                       continue
                   else:
                       self.message_port_pub(pmt.intern('rprt'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(RPRT_1), RPRT_1)))
                       continue
               else:
                   if cmd_list[2].isnumeric():
                       #print(int(cmd_list[2]))
                       if cmd_list[1].lower() == 'tx':
                           self.tx_freq = int(cmd_list[2])
                           print(self.tx_freq)
                           self.message_port_pub(pmt.intern('set_TX'), pmt.to_pmt({'freq':int(self.tx_freq)}))
                       elif cmd_list[1].lower() == 'rx':
                           self.rx_freq = int(cmd_list[2])
                           print(self.rx_freq)
                           self.message_port_pub(pmt.intern('set_RX'), pmt.to_pmt({'freq':int(self.rx_freq)}))
                       else:
                           self.message_port_pub(pmt.intern('rprt'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(RPRT_16), RPRT_16)))
                           continue
                       self.message_port_pub(pmt.intern('rprt'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(RPRT_0), RPRT_0)))
                       continue
                   else:
                       self.message_port_pub(pmt.intern('rprt'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(RPRT_1), RPRT_1)))
                       continue
           #elif(cmd_list[0] == 'q'):
            #   self.message_port_pub(pmt.intern('rprt'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(RPRT_0), RPRT_0))) 
     def handle_msg(self, msg_pmt):
         msg = pmt.cdr(msg_pmt)
         if not pmt.is_uniform_vector(msg):
             print("Not a vector")
             self.message_port_pub(pmt.intern('rprt'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(RPRT_6), RPRT_6)))
             return
         command = pmt.to_python(msg).tobytes().decode("ascii")
         #print(command)
         self.process_cmd(command)
         #self.message_port_pub(pmt.intern('rprt'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(RPRT_0), RPRT_0)))
