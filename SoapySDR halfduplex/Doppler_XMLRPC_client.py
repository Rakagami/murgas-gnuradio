import sys
import time
import datetime
from math import *
import ephem
#from satellite_tle import fetch_tle_from_celestrak
import xmlrpc.client

class Tracker():

    def __init__(self, satellite, groundstation=("59.4000", "24.8170", "0")):
        self.groundstation = ephem.Observer()
        self.groundstation.lat = groundstation[0]
        self.groundstation.lon = groundstation[1]
        self.groundstation.elevation = int(groundstation[2])

        self.satellite = ephem.readtle(satellite["name"], satellite["tle1"], satellite["tle2"])

    def set_epoch(self, epoch=time.time()):
        ''' sets epoch when parameters are observed '''

        self.groundstation.date = datetime.datetime.utcfromtimestamp(epoch)
        self.satellite.compute(self.groundstation)

    def azimuth(self):
        ''' returns satellite azimuth in degrees '''
        return degrees(self.satellite.az)

    def elevation(self):
        ''' returns satellite elevation in degrees '''
        return degrees(self.satellite.alt)

    def latitude(self):
        ''' returns satellite latitude in degrees '''
        return degrees(self.satellite.sublat)

    def longitude(self):
        ''' returns satellite longitude in degrees '''
        return degrees(self.satellite.sublong)

    def range(self):
        ''' returns satellite range in meters '''
        return self.satellite.range

    def doppler(self, frequency_hz=437025000):
        ''' returns doppler shift in hertz '''
        return -self.satellite.range_velocity / 299792458. * frequency_hz

    def ecef_coordinates(self):
        ''' returns satellite earth centered cartesian coordinates
            https://en.wikipedia.org/wiki/ECEF
        '''
        x, y, z = self._aer2ecef(self.azimuth(), self.elevation(), self.range(), float(self.groundstation.lat), float(self.groundstation.lon), self.groundstation.elevation)
        return x, y, z

    def _aer2ecef(self, azimuthDeg, elevationDeg, slantRange, obs_lat, obs_long, obs_alt):

        # site ecef in meters
        sitex, sitey, sitez = llh2ecef(obs_lat, obs_long, obs_alt)

        # some needed calculations
        slat = sin(radians(obs_lat))
        slon = sin(radians(obs_long))
        clat = cos(radians(obs_lat))
        clon = cos(radians(obs_long))

        azRad = radians(azimuthDeg)
        elRad = radians(elevationDeg)

        # az,el,range to sez convertion
        south = -slantRange * cos(elRad) * cos(azRad)
        east = slantRange * cos(elRad) * sin(azRad)
        zenith = slantRange * sin(elRad)

        x = (slat * clon * south) + (-slon * east) + (clat * clon * zenith) + sitex
        y = (slat * slon * south) + (clon * east) + (clat * slon * zenith) + sitey
        z = (-clat * south) + (slat * zenith) + sitez

        return x, y, z


if __name__ == "__main__":
    try:

        # taken from: http://celestrak.com/NORAD/elements/cubesat.txt
        #norad_id = 47959 # GRBAlpha
        f0 = 436025000   # Hz center frequency
        xmlrpc_endpoint = 'http://localhost:8888'
        #tle = fetch_tle_from_celestrak(norad_id)
        ec1_tle = {"name": "BDSat", 
                   "tle1": "1 52175U 22033U   22096.46166330  .00011561  00000-0  51098-3 0  9998",
                   "tle2": "2 52175  97.3893 179.2313 0012787 252.0859 107.8983 15.21794711   737"}

        # http://www.gpscoordinates.eu/show-gps-coordinates.php
        myloc = ("49.1286", "18.3256", "300")

        tracker = Tracker(satellite=ec1_tle, groundstation=myloc)

        xmlrpcDopplerFreq = xmlrpc.client.ServerProxy(xmlrpc_endpoint)
        while 1:
            tracker.set_epoch(time.time())
            print("az         : {:0.1f}".format(tracker.azimuth()))
            print("ele        : {:0.1f}".format(tracker.elevation()))
            print("range      : {:0.0f} km" .format((tracker.range()/1000)))
            print("range rate : {:0.3f} km/s" .format((tracker.satellite.range_velocity/1000)))
            print("doppler    : {:0.0f} Hz" .format((tracker.doppler(f0))))
            print("freq       : {:0.0f} Hz".format(f0+tracker.doppler(f0)))

            xmlrpcDopplerFreq.set_rx_freq(int(f0+tracker.doppler(f0)))
        
            time.sleep(5)

    except KeyboardInterrupt:
        print("\nExiting.\n")
            
