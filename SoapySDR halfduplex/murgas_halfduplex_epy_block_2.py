"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

import numpy as np
from gnuradio import gr
import pmt

class blk(gr.basic_block):  # other base classes are basic_block, decim_block, interp_block
    """Embedded Python Block example - a simple multiply const"""

    def __init__(self):  # only default arguments here
        """arguments to this function show up as parameters in GRC"""
        gr.basic_block.__init__(
            self,
            name='BDSat beacon',   # will show up in GRC
            in_sig=None,
            out_sig=None)

        self.message_port_register_in(pmt.intern('in'))
        self.set_msg_handler(pmt.intern('in'), self.handle_msg) 

    def handle_msg(self, msg_pmt):
        index = 0
        
        dest_callsign = ""
        source_callsign = ""
        dest_ssid = ""
        source_ssid = ""
        control = 0
        pid = 0
        payload = ""
        
        msg = pmt.cdr(msg_pmt)
        if not pmt.is_u8vector(msg):
            print('ERROR Invalid message type. Expected u8vector')
            return

        frame = bytes(pmt.u8vector_elements(msg))
        #print(frame)
        for i in range (6):
            dest_callsign += chr((frame[index] & 0xFF) >> 1)
            index += 1            
        dest_ssid += chr((frame[index] & 0xFF) >> 1)
        index += 1
      	
        for i in range (6):
            source_callsign += chr((frame[index] & 0xFF) >> 1)
            index += 1            
        source_ssid += chr((frame[index] & 0xFF) >> 1)
        index += 1

        control = frame[index] & 0xFF
        index += 1
        
        pid += frame[index] & 0xFF
        index += 1

        
        payload_len = len(frame) - index
        for i in range(payload_len):
            payload += chr(frame[index] & 0xFF)
            index += 1
        #print(payload)
        
        data = payload.split(',')
        if (data [0].isnumeric()):
            print("TRX beacon:\n")
            print("""Source callsign: {}\nDestination callsign: {}\nControl: {}\nPID:{}\n""".format(source_callsign, dest_callsign, control, pid))
            print("""Uptime: {}s\nTotal uptime: {}s\nResets: {}\nMCU temp: {}°C\nRF temp: {}°C\nPA temp: {}°C\nDigipeater msg count: {}\nLast digipeater user: {}\nReceived packets: {}\nTransmitted packets: {}\nActual RSSI: {}dBm\nDCD RSSI: {}dBm\n""".format(data[0], data[1], data[2], int(data[3])/100, int(data[4])/100, int(data[5])/100, data[6], data[7], data[8], data[9], int(data[10])/2-134, data[11]))
