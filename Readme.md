## Simple GNU Radio flowchart for Cubesat Space Protocol packet reception and communication with Spacemanic Murgas TRX radio

Originally created for GNU Radio 3.9

Dependencies:
- SoapySDR module (SDR input and output)
- gr-satellites by Daniel Estévez (Print CSP header, KISS Server, etc.) see: [https://github.com/daniestevez/gr-satellites](https://github.com/daniestevez/gr-satellites)

The `murgas-gnuradio.grc` flow shows an example of Spacemanic Murgas radio reception. The decoded frames are displayed and forwarded to a TCP KISS Server.

The `murgas-halfduplex.grc` flow shows an example of half-duplex communication with Spacemanuc Murgas radio module. The example uses a Nuand BladeRF SDR. The communication is done using two TCP KISS servers one for RX one for TX.

### gr-satellites observed necessary fixes
See components in *gr-satellites hotfixes*. The wo components used were observed to need small fixing. Kiss server block had incompatible variable type and *csp_header.py* had incompatible endianness.

![Murgas TRX GNU Radio flowchart](murgas-gnuradio-flowchart.png)
