#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Spacemanic commander
# Author: Spacemanic
# Copyright: Robeu Samo
# Description: Fullduplex Ettus SDR manager
# GNU Radio version: 3.10.1.1

from packaging.version import Version as StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from PyQt5 import Qt
from gnuradio import eng_notation
from gnuradio import qtgui
from gnuradio.filter import firdes
import sip
from gnuradio import blocks
from gnuradio import digital
from gnuradio import filter
from gnuradio import gr
from gnuradio.fft import window
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import gr, pdu
from gnuradio import network
from gnuradio import uhd
import time
from xmlrpc.server import SimpleXMLRPCServer
import threading
import ettus_fullduplex_dev_epy_module_0 as epy_module_0  # embedded python module
import math
import satellites
import satellites.components.demodulators


def snipfcn_snippet_0(self):
    ##################################################Self defined function##################################################
    # Description: Set B200Mini AUX I/O (GPIO)
    # Set GPIO7 to be automatically controlled by ATR as an output.
    # Set GPIO7 to be high when transmitting only, and low in all other cases.

    # set the masks, define the pin numbers
    AMP_GPIO_MASK = (1 << 7)
    ATR_MASKS =  AMP_GPIO_MASK

    # set the values for ATR control: 1 for ATR, 0 for manual
    ATR_CONTROL = AMP_GPIO_MASK

    # set the GPIO directions: 1 for output, 0 for input
    GPIO_DDR = AMP_GPIO_MASK


    # set basic ATR setup
    self.uhd_usrp_sink_0.set_gpio_attr("FP0", "CTRL", ATR_CONTROL, ATR_MASKS)
    self.uhd_usrp_sink_0.set_gpio_attr("FP0", "DDR", GPIO_DDR, ATR_MASKS)

    # set GPIO7 as described above
    self.uhd_usrp_sink_0.set_gpio_attr("FP0", "ATR_0X", 0, AMP_GPIO_MASK)
    self.uhd_usrp_sink_0.set_gpio_attr("FP0", "ATR_RX", 0, AMP_GPIO_MASK)
    self.uhd_usrp_sink_0.set_gpio_attr("FP0", "ATR_TX", 0, AMP_GPIO_MASK)
    self.uhd_usrp_sink_0.set_gpio_attr("FP0", "ATR_XX", (1<<7), AMP_GPIO_MASK)


def snippets_main_after_start(tb):
    snipfcn_snippet_0(tb)

from gnuradio import qtgui

class ettus_fullduplex_dev(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Spacemanic commander", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Spacemanic commander")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "ettus_fullduplex_dev")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Variables
        ##################################################
        self.tx_freq = tx_freq = 145925000
        self.samp_rate = samp_rate = 384000
        self.rx_freq = rx_freq = 436680000
        self.tx_gain = tx_gain = 0
        self.tx_freq_label = tx_freq_label = tx_freq
        self.taps = taps = firdes.low_pass(1.0, samp_rate, 30000,2000, window.WIN_HAMMING, 6.76)
        self.sps = sps = 40
        self.rx_offset = rx_offset = 50000
        self.rx_gain = rx_gain = 60
        self.rx_freq_label = rx_freq_label = rx_freq
        self.fix_rx_freq = fix_rx_freq = 436680000

        ##################################################
        # Blocks
        ##################################################
        _tx_gain_check_box = Qt.QCheckBox("TX Enable")
        self._tx_gain_choices = {True: 1, False: 0}
        self._tx_gain_choices_inv = dict((v,k) for k,v in self._tx_gain_choices.items())
        self._tx_gain_callback = lambda i: Qt.QMetaObject.invokeMethod(_tx_gain_check_box, "setChecked", Qt.Q_ARG("bool", self._tx_gain_choices_inv[i]))
        self._tx_gain_callback(self.tx_gain)
        _tx_gain_check_box.stateChanged.connect(lambda i: self.set_tx_gain(self._tx_gain_choices[bool(i)]))
        self.top_layout.addWidget(_tx_gain_check_box)
        self._tx_freq_tool_bar = Qt.QToolBar(self)
        self._tx_freq_tool_bar.addWidget(Qt.QLabel("TX freq" + ": "))
        self._tx_freq_line_edit = Qt.QLineEdit(str(self.tx_freq))
        self._tx_freq_tool_bar.addWidget(self._tx_freq_line_edit)
        self._tx_freq_line_edit.returnPressed.connect(
            lambda: self.set_tx_freq(eng_notation.str_to_num(str(self._tx_freq_line_edit.text()))))
        self.top_layout.addWidget(self._tx_freq_tool_bar)
        self._rx_gain_tool_bar = Qt.QToolBar(self)
        self._rx_gain_tool_bar.addWidget(Qt.QLabel("RX gain" + ": "))
        self._rx_gain_line_edit = Qt.QLineEdit(str(self.rx_gain))
        self._rx_gain_tool_bar.addWidget(self._rx_gain_line_edit)
        self._rx_gain_line_edit.returnPressed.connect(
            lambda: self.set_rx_gain(eng_notation.str_to_num(str(self._rx_gain_line_edit.text()))))
        self.top_layout.addWidget(self._rx_gain_tool_bar)
        self._fix_rx_freq_tool_bar = Qt.QToolBar(self)
        self._fix_rx_freq_tool_bar.addWidget(Qt.QLabel("RX Freq" + ": "))
        self._fix_rx_freq_line_edit = Qt.QLineEdit(str(self.fix_rx_freq))
        self._fix_rx_freq_tool_bar.addWidget(self._fix_rx_freq_line_edit)
        self._fix_rx_freq_line_edit.returnPressed.connect(
            lambda: self.set_fix_rx_freq(eng_notation.str_to_num(str(self._fix_rx_freq_line_edit.text()))))
        self.top_layout.addWidget(self._fix_rx_freq_tool_bar)
        self.xmlrpc_server_0 = SimpleXMLRPCServer(('', 9990), allow_none=True)
        self.xmlrpc_server_0.register_instance(self)
        self.xmlrpc_server_0_thread = threading.Thread(target=self.xmlrpc_server_0.serve_forever)
        self.xmlrpc_server_0_thread.daemon = True
        self.xmlrpc_server_0_thread.start()
        self.uhd_usrp_source_0 = uhd.usrp_source(
            ",".join(('', '')),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
        )
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0.set_time_now(uhd.time_spec(time.time()), uhd.ALL_MBOARDS)

        self.uhd_usrp_source_0.set_center_freq(fix_rx_freq-rx_offset, 0)
        self.uhd_usrp_source_0.set_antenna("RX2", 0)
        self.uhd_usrp_source_0.set_bandwidth(300000, 0)
        self.uhd_usrp_source_0.set_rx_agc(False, 0)
        self.uhd_usrp_source_0.set_gain(rx_gain, 0)
        self.uhd_usrp_source_0.set_auto_dc_offset(False, 0)
        self.uhd_usrp_source_0.set_auto_iq_balance(False, 0)
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
            ",".join(('', '')),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
            'tx_pkt',
        )
        self.uhd_usrp_sink_0.set_clock_source('internal', 0)
        self.uhd_usrp_sink_0.set_samp_rate(9600*sps)
        self.uhd_usrp_sink_0.set_time_now(uhd.time_spec(time.time()), uhd.ALL_MBOARDS)

        self.uhd_usrp_sink_0.set_center_freq(tx_freq, 0)
        self.uhd_usrp_sink_0.set_antenna("TX/RX", 0)
        self.uhd_usrp_sink_0.set_normalized_gain(tx_gain, 0)
        self._tx_freq_label_tool_bar = Qt.QToolBar(self)

        if None:
            self._tx_freq_label_formatter = None
        else:
            self._tx_freq_label_formatter = lambda x: eng_notation.num_to_str(x)

        self._tx_freq_label_tool_bar.addWidget(Qt.QLabel("Actual TX freq: "))
        self._tx_freq_label_label = Qt.QLabel(str(self._tx_freq_label_formatter(self.tx_freq_label)))
        self._tx_freq_label_tool_bar.addWidget(self._tx_freq_label_label)
        self.top_layout.addWidget(self._tx_freq_label_tool_bar)
        self.satellites_print_header_0 = satellites.print_header()
        self.satellites_pdu_to_kiss_0 = satellites.pdu_to_kiss(control_byte = True, include_timestamp = False)
        self.satellites_nrzi_encode_0 = satellites.nrzi_encode()
        self.satellites_nrzi_decode_0 = satellites.nrzi_decode()
        self.satellites_kiss_to_pdu_0 = satellites.kiss_to_pdu(True)
        self.satellites_hdlc_framer_0 = satellites.hdlc_framer(preamble_bytes=150, postamble_bytes=20)
        self.satellites_fsk_demodulator_0 = satellites.components.demodulators.fsk_demodulator(baudrate = 9600, samp_rate = 48000, iq = True, subaudio = False, options="")
        self._rx_freq_label_tool_bar = Qt.QToolBar(self)

        if None:
            self._rx_freq_label_formatter = None
        else:
            self._rx_freq_label_formatter = lambda x: eng_notation.num_to_str(x)

        self._rx_freq_label_tool_bar.addWidget(Qt.QLabel("Actual RX freq: "))
        self._rx_freq_label_label = Qt.QLabel(str(self._rx_freq_label_formatter(self.rx_freq_label)))
        self._rx_freq_label_tool_bar.addWidget(self._rx_freq_label_label)
        self.top_layout.addWidget(self._rx_freq_label_tool_bar)
        self.qtgui_sink_x_0 = qtgui.sink_c(
            8192, #fftsize
            window.WIN_BLACKMAN_hARRIS, #wintype
            rx_freq, #fc
            samp_rate, #bw
            "", #name
            True, #plotfreq
            True, #plotwaterfall
            False, #plottime
            False, #plotconst
            None # parent
        )
        self.qtgui_sink_x_0.set_update_time(1.0/10)
        self._qtgui_sink_x_0_win = sip.wrapinstance(self.qtgui_sink_x_0.qwidget(), Qt.QWidget)

        self.qtgui_sink_x_0.enable_rf_freq(True)

        self.top_layout.addWidget(self._qtgui_sink_x_0_win)
        self.pdu_pdu_to_tagged_stream_2 = pdu.pdu_to_tagged_stream(gr.types.byte_t, 'packet_len')
        self.pdu_pdu_to_tagged_stream_1 = pdu.pdu_to_tagged_stream(gr.types.byte_t, 'tx_pkt')
        self.network_socket_pdu_0 = network.socket_pdu('TCP_SERVER', '127.0.0.1', '9999', 10000, False)
        self.low_pass_filter_0 = filter.fir_filter_ccf(
            1,
            firdes.low_pass(
                1,
                samp_rate/8,
                8000,
                500,
                window.WIN_HAMMING,
                6.76))
        self.freq_xlating_fir_filter_xxx_0 = filter.freq_xlating_fir_filter_ccf(8, taps, rx_freq-fix_rx_freq+50000, samp_rate)
        self.digital_scrambler_bb_0 = digital.scrambler_bb(0x21, 0x00, 16)
        self.digital_hdlc_deframer_bp_0 = digital.hdlc_deframer_bp(8, 500)
        self.digital_gfsk_mod_0 = digital.gfsk_mod(
            samples_per_symbol=sps,
            sensitivity=(3000*2*math.pi)/(9600*sps),
            bt=0.5,
            verbose=False,
            log=False,
            do_unpack=False)
        self.digital_descrambler_bb_0 = digital.descrambler_bb(0x21, 0x00, 16)
        self.digital_binary_slicer_fb_0 = digital.binary_slicer_fb()
        self.blocks_tagged_stream_multiply_length_0 = blocks.tagged_stream_multiply_length(gr.sizeof_gr_complex*1, 'tx_pkt', sps)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_cc(0.7)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.digital_hdlc_deframer_bp_0, 'out'), (self.satellites_pdu_to_kiss_0, 'in'))
        self.msg_connect((self.digital_hdlc_deframer_bp_0, 'out'), (self.satellites_print_header_0, 'in'))
        self.msg_connect((self.network_socket_pdu_0, 'pdus'), (self.pdu_pdu_to_tagged_stream_2, 'pdus'))
        self.msg_connect((self.satellites_hdlc_framer_0, 'out'), (self.pdu_pdu_to_tagged_stream_1, 'pdus'))
        self.msg_connect((self.satellites_kiss_to_pdu_0, 'out'), (self.satellites_hdlc_framer_0, 'in'))
        self.msg_connect((self.satellites_pdu_to_kiss_0, 'out'), (self.network_socket_pdu_0, 'pdus'))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_tagged_stream_multiply_length_0, 0))
        self.connect((self.blocks_tagged_stream_multiply_length_0, 0), (self.uhd_usrp_sink_0, 0))
        self.connect((self.digital_binary_slicer_fb_0, 0), (self.satellites_nrzi_decode_0, 0))
        self.connect((self.digital_descrambler_bb_0, 0), (self.digital_hdlc_deframer_bp_0, 0))
        self.connect((self.digital_gfsk_mod_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.digital_scrambler_bb_0, 0), (self.satellites_nrzi_encode_0, 0))
        self.connect((self.freq_xlating_fir_filter_xxx_0, 0), (self.low_pass_filter_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.satellites_fsk_demodulator_0, 0))
        self.connect((self.pdu_pdu_to_tagged_stream_1, 0), (self.digital_scrambler_bb_0, 0))
        self.connect((self.pdu_pdu_to_tagged_stream_2, 0), (self.satellites_kiss_to_pdu_0, 0))
        self.connect((self.satellites_fsk_demodulator_0, 0), (self.digital_binary_slicer_fb_0, 0))
        self.connect((self.satellites_nrzi_decode_0, 0), (self.digital_descrambler_bb_0, 0))
        self.connect((self.satellites_nrzi_encode_0, 0), (self.digital_gfsk_mod_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.freq_xlating_fir_filter_xxx_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.qtgui_sink_x_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "ettus_fullduplex_dev")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_tx_freq(self):
        return self.tx_freq

    def set_tx_freq(self, tx_freq):
        self.tx_freq = tx_freq
        Qt.QMetaObject.invokeMethod(self._tx_freq_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.tx_freq)))
        self.set_tx_freq_label(self.tx_freq)
        self.uhd_usrp_sink_0.set_center_freq(self.tx_freq, 0)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_taps(firdes.low_pass(1.0, self.samp_rate, 30000, 2000, window.WIN_HAMMING, 6.76))
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate/8, 8000, 500, window.WIN_HAMMING, 6.76))
        self.qtgui_sink_x_0.set_frequency_range(self.rx_freq, self.samp_rate)
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)

    def get_rx_freq(self):
        return self.rx_freq

    def set_rx_freq(self, rx_freq):
        self.rx_freq = rx_freq
        self.set_rx_freq_label(self.rx_freq)
        self.freq_xlating_fir_filter_xxx_0.set_center_freq(self.rx_freq-self.fix_rx_freq+50000)
        self.qtgui_sink_x_0.set_frequency_range(self.rx_freq, self.samp_rate)

    def get_tx_gain(self):
        return self.tx_gain

    def set_tx_gain(self, tx_gain):
        self.tx_gain = tx_gain
        self._tx_gain_callback(self.tx_gain)
        self.uhd_usrp_sink_0.set_normalized_gain(self.tx_gain, 0)

    def get_tx_freq_label(self):
        return self.tx_freq_label

    def set_tx_freq_label(self, tx_freq_label):
        self.tx_freq_label = tx_freq_label
        Qt.QMetaObject.invokeMethod(self._tx_freq_label_label, "setText", Qt.Q_ARG("QString", str(self._tx_freq_label_formatter(self.tx_freq_label))))

    def get_taps(self):
        return self.taps

    def set_taps(self, taps):
        self.taps = taps
        self.freq_xlating_fir_filter_xxx_0.set_taps(self.taps)

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps
        self.blocks_tagged_stream_multiply_length_0.set_scalar(self.sps)
        self.uhd_usrp_sink_0.set_samp_rate(9600*self.sps)

    def get_rx_offset(self):
        return self.rx_offset

    def set_rx_offset(self, rx_offset):
        self.rx_offset = rx_offset
        self.uhd_usrp_source_0.set_center_freq(self.fix_rx_freq-self.rx_offset, 0)

    def get_rx_gain(self):
        return self.rx_gain

    def set_rx_gain(self, rx_gain):
        self.rx_gain = rx_gain
        Qt.QMetaObject.invokeMethod(self._rx_gain_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.rx_gain)))
        self.uhd_usrp_source_0.set_gain(self.rx_gain, 0)

    def get_rx_freq_label(self):
        return self.rx_freq_label

    def set_rx_freq_label(self, rx_freq_label):
        self.rx_freq_label = rx_freq_label
        Qt.QMetaObject.invokeMethod(self._rx_freq_label_label, "setText", Qt.Q_ARG("QString", str(self._rx_freq_label_formatter(self.rx_freq_label))))

    def get_fix_rx_freq(self):
        return self.fix_rx_freq

    def set_fix_rx_freq(self, fix_rx_freq):
        self.fix_rx_freq = fix_rx_freq
        Qt.QMetaObject.invokeMethod(self._fix_rx_freq_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.fix_rx_freq)))
        self.freq_xlating_fir_filter_xxx_0.set_center_freq(self.rx_freq-self.fix_rx_freq+50000)
        self.uhd_usrp_source_0.set_center_freq(self.fix_rx_freq-self.rx_offset, 0)




def main(top_block_cls=ettus_fullduplex_dev, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print("Error: failed to enable real-time scheduling.")

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()
    snippets_main_after_start(tb)
    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
