"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

import numpy as np
from gnuradio import gr
import pmt

FEND = np.uint8(0xc0)
FESC = np.uint8(0xdb)
TFEND = np.uint8(0xdc)
TFESC = np.uint8(0xdd)

def kiss_escape(a):
    """Escapes KISS control characters
    This replaces FEND and FESC according to the KISS escape rules
    """
    buff = list()
    trans = False
    for x in a:
       if x == FEND:
           if(buff):
               return buff[1:]
       elif trans:
           if x == TFEND:
               buff.append(FEND)
           elif x == TFESC:
               buff.append(FESC)
           trans = False
       elif x == FESC:
           trans = True
       else:
           buff.append(np.uint8(x))

class blk(gr.basic_block):  # other base classes are basic_block, decim_block, interp_block
    """Embedded Python Block example - a simple multiply const"""

    def __init__(self):  # only default arguments here
        """arguments to this function show up as parameters in GRC"""
        gr.basic_block.__init__(
            self,
            name='KISS Deframer',   # will show up in GRC
            in_sig=None,
            out_sig=None)

        self.message_port_register_in(pmt.intern('in'))
        self.message_port_register_out(pmt.intern('out'))
        self.set_msg_handler(pmt.intern('in'), self.handle_msg) 

    def handle_msg(self, msg_pmt):
        msg = pmt.cdr(msg_pmt)
        if not pmt.is_u8vector(msg):
            print('ERROR Invalid message type. Expected u8vector')
            return

        frame = kiss_escape(pmt.u8vector_elements(msg))
        if frame is None:
            print('ERROR Missing frame')
            return
        self.message_port_pub(pmt.intern('out'), pmt.cons(pmt.PMT_NIL, pmt.init_u8vector(len(frame), frame)))
        print(frame)
        return
